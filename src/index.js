import fs from 'fs';

let studentList = document.getElementById('studentList');
let nameInput = document.getElementById('nameInput');
let emailInput = document.getElementById('emailInput');
let addButton = document.getElementById('addButton');
let feedback = document.getElementById('feedback');

let students = [];

function updateStudentList() {
  while (studentList.firstChild) {
    studentList.removeChild(studentList.firstChild);
  }

  for (let c = 0; c < students.length; c++) {
    let li = document.createElement('li');
    li.innerText = students[c].name + ', ' + students[c].email;

    let button = document.createElement('button');
    button.innerText = 'x';
    button.onclick = () => {
      students.splice(c, 1);
      updateStudentList();
      feedback.innerText = 'Student removed!';
    };
    li.appendChild(button);
    studentList.appendChild(li);
  }
}
document.getElementById('loadButton').onclick = () => {
  fs.readFile('src/data.json', (error, data) => {
    if (error) {
      console.error('Failed to read students from file: ' + error.message);
      return;
    }
    students = JSON.parse(data);
    updateStudentList();
    feedback.innerText = 'Student list loaded!';
  });
};
document.getElementById('saveButton').onclick = () => {
  fs.writeFile('src/data.json', JSON.stringify(students), (error) => {
    if (error) {
      console.error('Failed to write students to file: ' + error.message);
      return;
    }
    feedback.innerText = 'Student list saved!';
  });
};

addButton.onclick = () => {
  students.push({ name: nameInput.value, email: emailInput.value });
  nameInput.value = '';
  emailInput.value = '';
  updateStudentList();
};
